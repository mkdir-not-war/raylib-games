#include "U_renderer.h"

U_Renderer::U_Renderer() {
    clearAllLayers();
}

U_Renderer::~U_Renderer() {
    clearAllLayers();
}

void U_Renderer::clearAllLayers() {
    for (int i=0; i<NUM_RENDERER_LAYERS; i++) {
        clearLayer(i);
    }
}

void U_Renderer::clearLayer(int layer) {
    // no need to deallocate any pointers because the 
    // memory is allocated inside the GameState when it
    // makes the pools/arrays of component ptrs

    _layers[layer].len = 0;
    for (int i=0; i<MAX_ENTITIES_PER_LAYER; i++) {
        _layers[layer].components[i] = nullptr;
    }
}

void U_Renderer::addEntity(int layer, CR_Render* component) {
    if (_layers[layer].len >= MAX_ENTITIES_PER_LAYER) {
        // ERROR: too many entities; cannot add any more
    }
    else {
        _layers[layer].components[_layers[layer].len] = component;
        _layers[layer].len++;
    }
}

void U_Renderer::removeEntity(int layer, CR_Render* component) {
    for (int i=0; i<_layers[layer].len; i++) {
        if (_layers[layer].components[i] == component) {
            _layers[layer].components[i] = nullptr;
        }
    }
    // ERROR: if this point is reached, the component does not exist in the layer
}

void U_Renderer::draw() {
    // layer zero is furthest background, layer five is closest foreground.
    for (int i=0; i<NUM_RENDERER_LAYERS; i++) {
        for (int j=0; j<_layers[i].len; j++) {
            _layers[i].components[j]->draw();
        }
    }
}