#include "CR_drawable.h"

CR_Drawable::CR_Drawable(
    void* texture, void* physicscomponent) : 
    _texture(texture),
    _physics(physicscomponent) {
    
}

CR_Drawable::~CR_Drawable() {
    // texture deallocation occurs in EntityFactory, so do nothing here
}

void CR_Drawable::draw() {
    // draw the texture at the position
    // position read from physics component
}