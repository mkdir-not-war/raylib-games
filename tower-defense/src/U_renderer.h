#ifndef RENDERER_H_8902375802
#define RENDERER_H_8902375802

#define MAX_ENTITIES_PER_LAYER  10000
#define NUM_RENDERER_LAYERS     5

#include "CR_render.h"

class U_Renderer {
struct layer {
    short len;
    CR_Render* components[MAX_ENTITIES_PER_LAYER];
};

private:
    layer _layers[NUM_RENDERER_LAYERS];

public:
    U_Renderer();
    ~U_Renderer();

    void clearLayer(int layer);
    void clearAllLayers();
    void addEntity(int layer, CR_Render* component);
    void removeEntity(int layer, CR_Render* component);

    void draw();
};

#endif