#ifndef CR_DRAWABLE_7890806
#define CR_DRAWABLE_7890806

#include "CR_render.h"

class CR_Drawable {
private:
    void* _texture;
    void* _physics;
public:
    CR_Drawable(void* texture, void* physicscomponent);
    ~CR_Drawable();

    void draw();
};

#endif