#include "raylib.h"
#include <stdio.h>  // sprintf

#include "U_renderer.h"

int screenWidth = 800;
int screenHeight = 450;

void InitGame(void);         
void UpdateGame(void);       
void DrawGame(void);         
void UnloadGame(void);       
void UpdateDrawFrame(void); 

//U_Renderer* renderer;

int main(void) {
    InitWindow(screenWidth, screenHeight, "FE Roguelike");
    InitGame();
    SetTargetFPS(120);

    while (!WindowShouldClose()) {
        UpdateDrawFrame();
    }

    UnloadGame();
    CloseWindow();
    return 0;
}

void InitGame(void)
{
    //renderer = new U_Renderer();
}

void UnloadGame(void)
{
    //delete renderer;
}

void UpdateGame(void)
{
}

void DrawGame(void)
{
    BeginDrawing();

    ClearBackground(RAYWHITE);

    char buff[20];
    snprintf(buff, sizeof(buff), "Current FPS: %d", GetFPS());
    DrawText(buff, 
        1, 
        screenHeight - 11, 
        10, 
        GRAY);

    //renderer->draw();

    EndDrawing();
}

void UpdateDrawFrame(void)
{
    UpdateGame();
    DrawGame();
}
