#ifndef RENDER_H_781510570
#define RENDER_H_781510570

class CR_Render {
public:
    CR_Render();
    ~CR_Render();

    virtual void draw() = 0;
};

#endif